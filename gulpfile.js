const gulp = require('gulp');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const prefixer = require('autoprefixer');
const rename = require("gulp-rename");
const uglify = require('gulp-uglify-es').default;
const del = require("del");
const sourcemaps = require('gulp-sourcemaps');
// const rigger = require('gulp-rigger');
const debug = require('gulp-debug');
const plumber = require('gulp-plumber');
const cssmin = require('gulp-minify-css');
const imagemin = require('gulp-imagemin');
const imageminJpegRecompress = require('imagemin-jpeg-recompress');
const pngquant = require('imagemin-pngquant');
const reload = browserSync.reload;
const postcss = require('gulp-postcss');
const newer = require('gulp-newer');

const path = {
  build: {
    html: 'dist/',
    js: 'dist/js/',
    css: 'dist/css/',
    img: 'dist/img/',
    fonts: 'dist/fonts/',
    libs: 'dist/libs/'
  },
  src: {
    html: 'src/*.*',
    js: 'src/js/**/*.js',
    style: 'src/sass/*.sass',
    libs: 'src/libs/**/*.*',
    css: 'src/css/*.css',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  watch: {
    html: 'src/*.*',
    template: 'src/template/*.html',
    js: 'src/js/**/*.js',
    sass: 'src/**/*.sass',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*',
    css: 'src/css/**/*.*',
    libs: 'src/libs/**/*.*'
  }
};

config = {
  server: {
    baseDir: "./dist"
  },
  logPrefix: "SERVER",
  open: true,
};

function webserver(done) {
  browserSync(config);
  done();
}

function browserSyncReload(done) {
  browsersync.reload();
  done();
}

function clean() {
  return del(["./_site/assets/"]);
}

function html() {
  return gulp.src(path.src.html)
    .pipe(newer(path.build.html))
    .pipe(debug({title: 'html:build ', showFiles: true}))
    .pipe(gulp.dest(path.build.html))
    .pipe(browserSync.stream());
}

function js() {
  return gulp.src(path.src.js)
  // .pipe(gulp.dest(path.build.js))
  // .pipe(uglify())
  // .pipe(rename({suffix: ".min"}))
    .pipe(newer(path.build.js))
    .pipe(debug({title: 'js:build ', showFiles: true}))
    .pipe(gulp.dest(path.build.js))
    .pipe(browserSync.stream());
}

function style() {
  return gulp.src(path.src.style)
    .pipe(newer(path.build.css))
    .pipe(plumber())
    .pipe(debug({title: 'style:build ', showFiles: true}))
    .pipe(sass())
    .pipe(postcss([prefixer()]))
    .pipe(gulp.dest(path.build.css))
    .pipe(cssmin())
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest(path.build.css))
    .pipe(browserSync.stream({match: '**/*.css'}));
}

function css() {
  return gulp.src(path.watch.css)
    .pipe(newer(path.build.css))
    .pipe(debug({title: 'css:build ', showFiles: true}))
    .pipe(postcss([prefixer()]))
    .pipe(gulp.dest(path.build.css))
    .pipe(postcss([prefixer()]))
    .pipe(cssmin())
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest(path.build.css));
}

function libs() {
  return gulp.src(path.watch.libs)
    .pipe(newer(path.build.libs))
    .pipe(debug({title: 'libs:build ', showFiles: true}))
    .pipe(gulp.dest(path.build.libs))
    .pipe(browserSync.stream());
}

function image() {
  return gulp.src(path.src.img) //Выберем наши картинки
    .pipe(plumber())
    .pipe(newer(path.build.img))
    .pipe(debug({title: 'building img:', showFiles: true}))
    .pipe(gulp.dest(path.build.img)) //Копируем изображения заранее, imagemin может пропустить парочку )
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imageminJpegRecompress({
        progressive: true,
        max: 80,
        min: 70
      }),
      pngquant({quality: '80'})
    ]))
    .pipe(gulp.dest(path.build.img))
    .pipe(browserSync.stream());
}

function fonts() {
  return gulp.src(path.src.fonts)
    .pipe(newer(path.build.fonts))
    .pipe(debug({title: 'fonts:build ', showFiles: true}))
    .pipe(gulp.dest(path.build.fonts))
}

function watchFiles() {
  gulp.watch(path.watch.html, html);
  gulp.watch(path.watch.template, html);
  gulp.watch(path.watch.sass, style);
  gulp.watch(path.watch.js, js);
  gulp.watch(path.watch.img, image);
  gulp.watch(path.watch.fonts, fonts);
  gulp.watch(path.watch.css, css);
  gulp.watch(path.watch.libs, libs);
}

// define complex tasks
const build = gulp.series(clean, gulp.parallel(html, style, image, js));
const watch = gulp.parallel(watchFiles, webserver);

// export tasks
exports.image = image;
exports.css = css;
exports.js = js;
exports.clean = clean;
exports.html = html;
exports.watch = watch;
exports.default = build;
